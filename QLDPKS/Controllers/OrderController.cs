﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Controllers
{
    public class OrderController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public OrderController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult SubmitBook(string fullname, string email, string category_room, string room, DateTime received_date, DateTime checkout_date, int numberofguest)
        {
            decimal price = _dbContext.Rooms.Where(x => x.RoomID == room).FirstOrDefault().RoomPrice;
            if(received_date < DateTime.Now && checkout_date <= DateTime.Now)
            {
                return Json(new { data = "Thời gian không hợp lệ.", status = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string customerID = Session["CustomerID"] as string;
                Order order = new Order
                {
                    OrderID = RandomString.getRandomString(5),
                    CustomerID = (customerID != null) ? customerID : RandomString.getRandomString(5),
                    RoomID = room,
                    Email = email,
                    ReceivedDate = received_date,
                    CheckOutDate = checkout_date,
                    NumberGuest = numberofguest,
                    Amount = price,
                    Status = false
                };
                var create = _dbContext.Orders.Add(order);

                if (create != null)
                {
                    _dbContext.SaveChanges();
                    return Json(new { data = "Đặt phòng thành công, vui lòng kiểm tra Mail", status = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = "Có lỗi xảy ra, vui lòng đợi", status = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}