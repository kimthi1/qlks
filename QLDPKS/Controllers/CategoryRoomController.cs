﻿using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Controllers
{
    public class CategoryRoomController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public CategoryRoomController()
        {
            _dbContext = new QLKSDatabaseContext();
        }
        // GET: CategoryRoom
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult getCategoryRoom()
        {
            var listCategoryRoom = _dbContext.CategoryRooms.OrderBy(x => x.NumberOfGuest).ToList();

            return Json(new { data = listCategoryRoom }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult getNumberOfGuest(string id)
        {
            var dataCategoryRoom = _dbContext.CategoryRooms.Where(x => x.CategoryRoomID == id).FirstOrDefault();
            return Json(new { data = dataCategoryRoom }, JsonRequestBehavior.AllowGet);
        }
    }
}