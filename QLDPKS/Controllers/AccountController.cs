﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Controllers
{
    public class AccountController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public AccountController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Login(string username, string password)
        {
            string hash_password = StringHash.crypto(password);
            var check = _dbContext.Customers.Where(x => x.CustomerEmail == username && x.CustomerPassword == hash_password).FirstOrDefault();

            if (check != null)
            {
                Session["CustomerID"] = check.CustomerID.ToString();
                return Json(new { data = "Đăng nhập thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Sai tên đăng nhập hoặc mật khẩu", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Register(string username, string password, string email, string address, string cellphone)
        {
            var check_email = _dbContext.Customers.Where(x => x.CustomerEmail == email).FirstOrDefault();

            if(check_email == null)
            {
                string hash_password = StringHash.crypto(password);

                Customer customer = new Customer
                {
                    CustomerID = RandomString.getRandomString(5),
                    CustomerName = username,
                    CustomerPassword = hash_password,
                    CustomerEmail = email,
                    CustomerAddress = address,
                    CustomerCellphone = cellphone
                };

                var register = _dbContext.Customers.Add(customer);

                if (register != null)
                {
                    _dbContext.SaveChanges();
                    return Json(new { data = "Đăng ký thành công", status = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = "Đăng ký thất bại", status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { data = "Email đã tồn tại, vui lòng nhập lại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAccount]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}