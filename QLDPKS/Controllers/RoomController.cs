﻿using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Controllers
{
    public class RoomController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public RoomController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult getRoom(string id)
        {
            var dataRoom = _dbContext.Rooms.Where(x => x.CategoryRoomID == id && x.Status == false).ToList();

            return Json(new { data = dataRoom }, JsonRequestBehavior.AllowGet);
        }
    }
}