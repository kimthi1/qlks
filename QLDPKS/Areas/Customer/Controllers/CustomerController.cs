﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Customer.Controllers
{
    public class CustomerController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public CustomerController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpPost]
        [AuthorizeAccount]
        public JsonResult UpdateCustomer(string address, string cellphone)
        {
            string customerID = (string)Session["CustomerID"];
            var updateCustomer = _dbContext.Customers.Where(x => x.CustomerID == customerID).FirstOrDefault();

            if(updateCustomer != null)
            {
                updateCustomer.CustomerAddress = address;
                updateCustomer.CustomerCellphone = cellphone;

                _dbContext.SaveChanges();
                return Json(new { data = "Cập nhật thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Cập nhật thất bại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAccount]
        public ActionResult ChangePassword()
        {
            if (Session != null || (Session)["CustomerID"] != null || (Session)["CustomerID"].ToString() != "")
            {
                string customerID = Session["CustomerID"] as string;
                var dataUser = _dbContext.Customers.Where(x => x.CustomerID == customerID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpPost]
        [AuthorizeAccount]
        public JsonResult ChangePassword(string old_password, string new_password)
        {
            string hash_password = StringHash.crypto(old_password);
            string customerID = (string)Session["CustomerID"];
            var updatePasswordCustomer = _dbContext.Customers.Where(x => x.CustomerID == customerID && x.CustomerPassword == hash_password).FirstOrDefault();

            if(updatePasswordCustomer != null)
            {
                string hash_new_password = StringHash.crypto(new_password);
                updatePasswordCustomer.CustomerPassword = hash_new_password;
                _dbContext.SaveChanges();

                return Json(new { data = "Đổi mật khẩu thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Mật khẩu cũ không chính xác", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAccount]
        public ActionResult Order()
        {
            if (Session != null || (Session)["CustomerID"] != null || (Session)["CustomerID"].ToString() != "")
            {
                string customerID = Session["CustomerID"] as string;
                var dataUser = _dbContext.Customers.Where(x => x.CustomerID == customerID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpPost]
        [AuthorizeAccount]
        public JsonResult Order(bool? flag)
        {
            string customerID = Session["CustomerID"] as string;

            var dataOrder = from a in _dbContext.Customers
                            join b in _dbContext.Orders
                            on a.CustomerID equals b.CustomerID
                            join c in _dbContext.Rooms
                            on b.RoomID equals c.RoomID
                            join d in _dbContext.CategoryRooms
                            on c.CategoryRoomID equals d.CategoryRoomID
                            where a.CustomerID == customerID
                            select new
                            {
                                CustomerName = a.CustomerName,
                                RoomName = c.RoomName,
                                CategoryRoomName = d.CategoryRoomName,
                                ReceivedDate = b.ReceivedDate,
                                CheckOutDate = b.CheckOutDate,
                                NumberOfGuest = b.NumberGuest,
                                Amount = b.Amount,
                                Status = b.Status
                            };

            return Json(new { data = dataOrder }, JsonRequestBehavior.AllowGet);
        }
    }
}