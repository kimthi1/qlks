﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Customer.Controllers
{
    public class HomeController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public HomeController()
        {
            _dbContext = new QLKSDatabaseContext();
        }
        [HttpGet]
        [AuthorizeAccount]
        public ActionResult Index()
        {
            if (Session != null || (Session)["CustomerID"] != null || (Session)["CustomerID"].ToString() != "")
            {
                string customerID = Session["CustomerID"] as string;
                var dataUser = _dbContext.Customers.Where(x => x.CustomerID == customerID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }
    }
}