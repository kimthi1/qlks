﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Admin.Controllers
{
    public class OrderController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public OrderController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult getOrder()
        {
            var data_query = from a in _dbContext.Orders
                             join b in _dbContext.Rooms
                             on a.RoomID equals b.RoomID
                             join c in _dbContext.CategoryRooms
                             on b.CategoryRoomID equals c.CategoryRoomID
                             where a.Status == false
                             select new
                             {
                                 OrderID = a.OrderID,
                                 Email = a.Email,
                                 RoomName = b.RoomName,
                                 CategoryRoomName = c.CategoryRoomName,
                                 ReceivedDate = a.ReceivedDate,
                                 CheckOutDate = a.CheckOutDate,
                                 Amount = a.Amount,
                                 Status = a.Status
                             };
            return Json(new { data = data_query }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult approveOrder(string id)
        {
            var dataOrder = _dbContext.Orders.Where(x => x.OrderID == id).FirstOrDefault();

            if (dataOrder != null)
            {
                string roomID = dataOrder.RoomID;
                var dataRoom = _dbContext.Rooms.Where(x => x.RoomID == roomID).FirstOrDefault();

                dataOrder.Status = true;
                dataRoom.Status = true;

                _dbContext.SaveChanges();

                return Json(new { data = "Duyệt đơn thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Duyệt đơn không thành công", status = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}