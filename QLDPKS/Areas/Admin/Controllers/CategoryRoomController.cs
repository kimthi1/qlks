﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Admin.Controllers
{
    public class CategoryRoomController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public CategoryRoomController()
        {
            _dbContext = new QLKSDatabaseContext();
        }
        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult getCategoryRoom()
        {
            var dataCategoryRoom = _dbContext.CategoryRooms.ToList();

            return Json(new { data = dataCategoryRoom }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Create()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Create(string categoryName, int numberOfGuest)
        {
            CategoryRoom categoryRoom = new CategoryRoom
            {
                CategoryRoomID = RandomString.getRandomString(5),
                CategoryRoomName = categoryName,
                NumberOfGuest = numberOfGuest
            };

            var create = _dbContext.CategoryRooms.Add(categoryRoom);

            if(create != null)
            {
                try
                {
                    _dbContext.SaveChanges();
                    return Json(new { data = "Thêm thành công", status = true }, JsonRequestBehavior.AllowGet);
                }
                catch(DbEntityValidationException e)
                {
                    return Json(new { data = e.Message, status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { data = "Thêm thất bại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Edit(string id)
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            var categoryRoomData = _dbContext.CategoryRooms.Where(x => x.CategoryRoomID == id).FirstOrDefault();
            ViewBag.categoryRoomData = categoryRoomData;
            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Edit(string categoryName, int numberOfGuest, string id)
        {
            var check = _dbContext.CategoryRooms.Where(x => x.CategoryRoomID == id).FirstOrDefault();
            if(check != null)
            {
                check.CategoryRoomName = categoryName;
                check.NumberOfGuest = numberOfGuest;
                _dbContext.SaveChanges();

                return Json(new { data = "Cập nhật thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Loại phòng không tồn tại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Delete(string id)
        {
            var check = _dbContext.CategoryRooms.Where(x => x.CategoryRoomID == id).FirstOrDefault();
            if (check != null)
            {
                _dbContext.CategoryRooms.Remove(check);
                _dbContext.SaveChanges();

                return Json(new { data = "Xoá thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Loại phòng không tồn tại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}