﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public HomeController()
        {
            _dbContext = new QLKSDatabaseContext();
        }
        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Index()
        {
            if (Session != null || (Session)["Admin"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Login", "Account", new { area = "Admin" });
            }

            return View();
        }
    }
}