﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Admin.Controllers
{
    public class RoomController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public RoomController()
        {
            _dbContext = new QLKSDatabaseContext();
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult getRoom()
        {
            var data_query = from a in _dbContext.Rooms
                             join b in _dbContext.CategoryRooms
                             on a.CategoryRoomID equals b.CategoryRoomID
                             select new
                             {
                                 RoomID = a.RoomID,
                                 RoomName = a.RoomName,
                                 CategoryRoomID = b.CategoryRoomID,
                                 CategoryRoomName = b.CategoryRoomName,
                                 RoomPrice = a.RoomPrice,
                                 RoomStatus = a.Status
                             };
            return Json(new { data = data_query }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult getCategoryRoom(bool? flag)
        {
            var data_categoryRoom = _dbContext.CategoryRooms.ToList();
            return Json(new { data = data_categoryRoom, status = true }, JsonRequestBehavior.AllowGet);
        }
        

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Create()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Create(string room_name, string category_room, int price_room)
        {
            Room room = new Room
            {
                RoomID = RandomString.getRandomString(5),
                RoomName = room_name.ToString(),
                CategoryRoomID = category_room.ToString(),
                RoomPrice = decimal.Parse(price_room.ToString()),
                Status = false
            };

            var create = _dbContext.Rooms.Add(room);

            if (create != null)
            {
                try
                {
                    _dbContext.SaveChanges();
                    return Json(new { data = "Thêm thành công", status = true }, JsonRequestBehavior.AllowGet);
                }
                catch (DbEntityValidationException e)
                {
                    return Json(new { data = e.Message, status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { data = "Thêm thất bại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Edit(string id)
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            var roomData = _dbContext.Rooms.Where(x => x.RoomID == id).FirstOrDefault();
            ViewBag.roomData = roomData;
            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Edit(string room_name, string category_room, decimal price_room, string id)
        {
            var check = _dbContext.Rooms.Where(x => x.RoomID == id).FirstOrDefault();
            if (check != null)
            {
                check.RoomName = room_name;
                check.RoomPrice = price_room;
                check.CategoryRoomID = category_room;
                _dbContext.SaveChanges();

                return Json(new { data = "Cập nhật thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Loại phòng không tồn tại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult Delete(string id)
        {
            var check = _dbContext.Rooms.Where(x => x.RoomID == id).FirstOrDefault();
            if (check != null)
            {
                _dbContext.Rooms.Remove(check);
                _dbContext.SaveChanges();

                return Json(new { data = "Xoá thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Loại phòng không tồn tại", status = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Detail(String id)
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            var roomData = _dbContext.Rooms.Where(x => x.RoomID == id).FirstOrDefault();
            var orderData = _dbContext.Orders.Where(x => x.RoomID == id).FirstOrDefault();
            ViewBag.roomData = roomData;
            ViewBag.orderData = orderData;
            return View();
        }
    }
}