﻿using QLDPKS.Cores;
using QLDPKS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLDPKS.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        private readonly QLKSDatabaseContext _dbContext;

        public AdminController()
        {
            _dbContext = new QLKSDatabaseContext();
        }
        [HttpGet]
        [AuthorizeAdmin]

        public ActionResult ChangePassword()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpPost]
        [AuthorizeAdmin]
        public JsonResult ChangePassword(string old_password, string new_password)
        {
            string hash_password = StringHash.crypto(old_password);
            string adminID = (string)Session["AdminID"];
            var updatePasswordAdmin = _dbContext.Admins.Where(x => x.ID == adminID && x.Password == hash_password).FirstOrDefault();

            if (updatePasswordAdmin != null)
            {
                string hash_new_password = StringHash.crypto(new_password);
                updatePasswordAdmin.Password = hash_new_password;
                _dbContext.SaveChanges();

                return Json(new { data = "Đổi mật khẩu thành công", status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { data = "Mật khẩu cũ không chính xác", status = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult CategoryRoom()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Room()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpGet]
        [AuthorizeAdmin]
        public ActionResult Order()
        {
            if (Session != null || (Session)["AdminID"] != null || (Session)["AdminID"].ToString() != "")
            {
                string adminID = Session["AdminID"] as string;
                var dataUser = _dbContext.Admins.Where(x => x.ID == adminID).FirstOrDefault();
                ViewBag.dataUser = dataUser;
            }
            else
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }
    }
}