﻿function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
        return false;
    }
    else {
        return true;
    }
}

var errMess = function (input_type, alert) {
    let wrapper = $("form input[id='" + input_type + "']")
    if (!$("form").find(".alert-padding").length) {
        $(wrapper).after($("<div class=\"alert-padding\"><div class=\"alert alert-danger\" role=\"alert\">" + alert + "</div></div>"))
    }
    else {
        $("form").find(".alert-padding").remove()
    }
}

var errMessName = function (input_type, alert) {
    let wrapper = $("form input[name='" + input_type + "']")
    if (!$("form").find(".alert-padding").length) {
        $(wrapper).after($("<div class=\"alert-padding\"><div class=\"alert alert-danger\" role=\"alert\">" + alert + "</div></div>"))
    }
    else {
        $("form").find(".alert-padding").remove()
    }
}

var errMessSelect = function (input_type, alert) {
    let wrapper = $("form select[id='" + input_type + "']")
    if (!$("form").find(".alert-padding").length) {
        $(wrapper).after($("<div class=\"alert-padding\"><div class=\"alert alert-danger\" role=\"alert\">" + alert + "</div></div>"))
    }
    else {
        $("form").find(".alert-padding").remove()
    }
}