﻿namespace QLDPKS.Migrations
{
    using QLDPKS.Cores;
    using QLDPKS.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<QLDPKS.Models.QLKSDatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(QLDPKS.Models.QLKSDatabaseContext context)
        {
            //context.CategoryRooms.AddOrUpdate(
            //    new CategoryRoom() { CategoryRoomID = RandomString.getRandomString(5), CategoryRoomName = "Phòng đơn", NumberOfGuest = 1 },
            //    new CategoryRoom() { CategoryRoomID = RandomString.getRandomString(5), CategoryRoomName = "Phòng đôi", NumberOfGuest = 2 }
            //    );

            //context.Rooms.AddOrUpdate(
            //    new Room() { RoomID = RandomString.getRandomString(5), RoomName = "Phòng A05.01", RoomPrice = 250000, CategoryRoomID = "6MZIA", Status = true },
            //    new Room() { RoomID = RandomString.getRandomString(5), RoomName = "Phòng B01.02", RoomPrice = 100000, CategoryRoomID = "M1DHB", Status = true },
            //    new Room() { RoomID = RandomString.getRandomString(5), RoomName = "Phòng B11.03", RoomPrice = 105000, CategoryRoomID = "M1DHB", Status = true },
            //    new Room() { RoomID = RandomString.getRandomString(5), RoomName = "Phòng C03.02", RoomPrice = 5000, CategoryRoomID = "6MZIA", Status = true }
            //    );

            context.Admins.AddOrUpdate(
                new Admin() { ID = RandomString.getRandomString(5), Username = "admin", Password = StringHash.crypto("12345678"), Email = "abc@gmail.com"}
                );
        }
    }
}
