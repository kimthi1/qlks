namespace QLDPKS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class V1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CategoryRooms",
                c => new
                    {
                        CategoryRoomID = c.String(nullable: false, maxLength: 128),
                        CategoryRoomName = c.String(nullable: false),
                        NumberOfGuest = c.Int(nullable: false),
                        Room_RoomID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CategoryRoomID)
                .ForeignKey("dbo.Rooms", t => t.Room_RoomID)
                .Index(t => t.Room_RoomID);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ContactID = c.String(nullable: false, maxLength: 128),
                        ContactName = c.String(nullable: false),
                        ContactTel = c.String(nullable: false),
                        ContactEmail = c.String(nullable: false),
                        ContactMessage = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ContactID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(nullable: false),
                        CustomerPassword = c.String(nullable: false),
                        CustomerEmail = c.String(nullable: false),
                        CustomerAddress = c.String(nullable: false),
                        CustomerCellphone = c.String(nullable: false),
                        Order_OrderID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CustomerID)
                .ForeignKey("dbo.Orders", t => t.Order_OrderID)
                .Index(t => t.Order_OrderID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.String(nullable: false, maxLength: 128),
                        CustomerID = c.String(nullable: false),
                        RoomID = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        ReceivedDate = c.DateTime(nullable: false),
                        CheckOutDate = c.DateTime(nullable: false),
                        NumberGuest = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        RoomID = c.String(nullable: false, maxLength: 128),
                        RoomName = c.String(nullable: false),
                        RoomPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CategoryRoomID = c.String(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Order_OrderID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.RoomID)
                .ForeignKey("dbo.Orders", t => t.Order_OrderID)
                .Index(t => t.Order_OrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "Order_OrderID", "dbo.Orders");
            DropForeignKey("dbo.CategoryRooms", "Room_RoomID", "dbo.Rooms");
            DropForeignKey("dbo.Customers", "Order_OrderID", "dbo.Orders");
            DropIndex("dbo.Rooms", new[] { "Order_OrderID" });
            DropIndex("dbo.Customers", new[] { "Order_OrderID" });
            DropIndex("dbo.CategoryRooms", new[] { "Room_RoomID" });
            DropTable("dbo.Rooms");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
            DropTable("dbo.Contacts");
            DropTable("dbo.CategoryRooms");
            DropTable("dbo.Admins");
        }
    }
}
