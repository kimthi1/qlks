﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("CategoryRooms")]
    public class CategoryRoom
    {
        [Key]
        [Column("CategoryRoomID")]
        [Required]
        public string CategoryRoomID { get; set; }

        [Column("CategoryRoomName")]
        [Required]
        public string CategoryRoomName { get; set; }

        [Column("NumberOfGuest")]
        [Required]
        public int NumberOfGuest { get; set; }
    }
}