﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        [Column("OrderID")]
        [Required]
        public string OrderID { get; set; }

        [Column("CustomerID")]
        [Required]
        public string CustomerID { get; set; }

        [Column("RoomID")]
        [Required]
        public string RoomID { get; set; }

        [Column("Email")]
        [Required]
        public string Email { get; set; }

        [Column("ReceivedDate")]
        [Required]
        public DateTime ReceivedDate { get; set; }

        [Column("CheckOutDate")]
        [Required]
        public DateTime CheckOutDate { get; set; }

        [Column("NumberGuest")]
        [Required]
        public int NumberGuest { get; set; }

        [Column("Amount")]
        [Required]
        public decimal Amount { get; set; }

        [Column("Status")]
        [Required]
        public bool Status { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
    }
}