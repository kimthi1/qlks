﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        [Column("CustomerID")]
        [Required]
        public string CustomerID { get; set; }

        [Column("CustomerName")]
        [Required]
        public string CustomerName { get; set; }

        [Column("CustomerPassword")]
        [Required]
        public string CustomerPassword { get; set; }

        [Column("CustomerEmail")]
        [Required]
        public string CustomerEmail { get; set; }

        [Column("CustomerAddress")]
        [Required]
        public string CustomerAddress { get; set; }

        [Column("CustomerCellphone")]
        [Required]
        public string CustomerCellphone { get; set; }
    }
}