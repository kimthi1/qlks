﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("Rooms")]
    public class Room
    {
        [Key]
        [Column("RoomID")]
        [Required]
        public string RoomID { get; set; }

        [Column("RoomName")]
        [Required]
        public string RoomName { get; set; }

        [Column("RoomPrice")]
        [Required]
        public decimal RoomPrice { get; set; }

        [Column("CategoryRoomID")]
        [Required]
        public string CategoryRoomID { get; set; }

        [Column("Status")]
        [Required]
        public bool Status { get; set; }

        public virtual ICollection<CategoryRoom> CategoryRooms { get; set; }
    }
}