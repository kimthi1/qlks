﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("Contacts")]
    public class Contact
    {
        [Key]
        [Column("ContactID")]
        [Required]
        public string ContactID { get; set; }

        [Column("ContactName")]
        [Required]
        public string ContactName { get; set; }

        [Column("ContactTel")]
        [Required]
        public string ContactTel { get; set; }

        [Column("ContactEmail")]
        [Required]
        public string ContactEmail { get; set; }

        [Column("ContactMessage")]
        [Required]
        public string ContactMessage { get; set; }
    }
}