﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace QLDPKS.Models
{
    public class QLKSDatabaseContext : DbContext
    {
        public QLKSDatabaseContext() : base("name=QLKSConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<QLKSDatabaseContext, QLDPKS.Migrations.Configuration>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<QLKSDatabaseContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<CategoryRoom> CategoryRooms { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Contact> Contacts { get; set; }
    }
}