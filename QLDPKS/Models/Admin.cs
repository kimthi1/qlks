﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QLDPKS.Models
{
    [Table("Admins")]
    public class Admin
    {
        [Key]
        [Column("ID")]
        [Required]
        public string ID { get; set; }

        [Column("Username")]
        [Required]
        public string Username { get; set; }

        [Column("Password")]
        [Required]
        public string Password { get; set; }

        [Column("Email")]
        [Required]
        public string Email { get; set; }
    }
}